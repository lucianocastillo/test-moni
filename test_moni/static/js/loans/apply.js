/*
 * Validates a loan with the form data and notifies the user.
 */
function validate_loan() {
  $.post(
    'validate/',
    {
      document_number: $('#id_document_number').val(),
      first_name: $('#id_first_name').val(),
      last_name: $('#id_last_name').val(),
      gender: $('#id_gender').val(),
      email: $('#id_email').val(),
      amount: $('#id_amount').val()
    }
  ).done(function(json) {
    if(json.is_approved)
      $('#msg').html('YOUR LOAN WAS APPROVED!');
    else
      $('#msg').html('YOUR LOAN WAS REJECTED!');
  }).fail(function(jqXHR, textStatus, errorThrown) {
    alert(errorThrown);
  });
}

$('#form').on('submit', function(event){
  event.preventDefault();
  validate_loan();
});
