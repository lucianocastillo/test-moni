from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings

from .models import LoanAdmin


class LoanAdminAccountAdapter(DefaultAccountAdapter):

    def is_open_for_signup(self, request):
        """
        Check the site is open for signup.
        """
        return getattr(settings, 'OPEN_FOR_SIGNUP', True)

    def save_user(self, request, user, form, commit=True):
        """
        Saves a new LoanAdmin instance.
        """
        user = super(LoanAdminAccountAdapter, self).save_user(
            request, user, form, commit
        )
        LoanAdmin.objects.create(
            user=user
        )
        return user
