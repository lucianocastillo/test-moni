from django import forms

from .models import Loan


class LoanModelForm(forms.ModelForm):

    class Meta:
        model = Loan
        fields = (
            'document_number',
            'first_name',
            'last_name',
            'gender',
            'email',
            'amount',
        )
