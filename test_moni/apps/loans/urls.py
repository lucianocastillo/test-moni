from django.contrib.auth.decorators import login_required
from django.urls import include, path
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='apply_loan')),

    path('loans/apply/', include([
        path('', views.LoanFormView.as_view(), name='apply_loan'),
        path('validate/', views.LoanValidatorCreateAPIView.as_view(), name='validate_loan'),
    ])),

    # Admin site urls
    path('admin/', include([
        path('', RedirectView.as_view(pattern_name='list_loans')),

        path('loans/', login_required(views.LoanListView.as_view()), name='list_loans'),

        path('loans/<int:pk>/', include([
            path('', login_required(views.LoanDetailView.as_view()), name='detail_loan'),
            path('edit/', login_required(views.LoanUpdateView.as_view()), name='update_loan'),
            path('delete/', login_required(views.LoanDeleteView.as_view()), name='delete_loan'),
        ])),
    ])),
]
