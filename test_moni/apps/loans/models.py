from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse_lazy


class Loan(models.Model):
    """
    Represents a loan.
    """
    APPROVED_CHOICES = (
        (True, 'Approved'),
        (False, 'Not approved'),
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    is_active = models.BooleanField(default=True)
    is_approved = models.BooleanField(default=False, choices=APPROVED_CHOICES)

    document_number = models.PositiveIntegerField()
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    email = models.EmailField(max_length=128)
    amount = models.DecimalField(decimal_places=2, max_digits=16)

    def delete(self):
        self.is_active = False
        self.save()

    def get_absolute_url(self):
        return reverse_lazy('list_loans')


class LoanAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
