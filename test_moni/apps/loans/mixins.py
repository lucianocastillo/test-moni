from django.contrib.auth.mixins import UserPassesTestMixin

from .models import LoanAdmin


class LoanAdminLoginMixin(UserPassesTestMixin):
    """
    Check the current user is a LoanAdmin.
    """
    def test_func(self):
        return (
            hasattr(self.request.user, 'loanadmin') and
            isinstance(self.request.user.loanadmin, LoanAdmin)
        )
