from rest_framework import serializers
from .models import Loan


class LoanModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = (
            'is_approved',
            'document_number',
            'first_name',
            'last_name',
            'gender',
            'email',
            'amount'
        )
