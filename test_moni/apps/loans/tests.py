from django.test import TestCase
from django.urls import reverse_lazy

from .models import Loan


class LoanTestCase(TestCase):
    def setUp(self):
        Loan.objects.create(
            document_number=11111111,
            first_name='Charles',
            last_name='Bukowski',
            gender='M',
            email='buko@gmail.com',
            amount=70000
        )

    def test_absolute_url(self):
        """
        Test the 'get_absolute_url' method.
        """
        loan = Loan.objects.get(document_number=11111111)
        abs_url = loan.get_absolute_url()

        self.assertEqual(abs_url, reverse_lazy('list_loans'))

    def test_loan_delete(self):
        """
        Test deletion just deactivate loan object.
        """
        loan = Loan.objects.get(document_number=11111111)
        loan.delete()

        self.assertFalse(loan.is_active)
