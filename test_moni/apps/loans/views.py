import requests

from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, FormView, UpdateView
from django.views.generic.list import ListView

from rest_framework.generics import CreateAPIView

from .forms import LoanModelForm
from .mixins import LoanAdminLoginMixin
from .models import Loan
from .serializers import LoanModelSerializer


class LoanValidatorCreateAPIView(CreateAPIView):
    """
    Validates a loan by an external api.
    """
    BASE_URL_API = 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/'

    queryset = Loan.objects.all()
    serializer_class = LoanModelSerializer

    def perform_create(self, serializer):
        # Get the parameters needed to make the api call
        params = {
            'document_number': serializer.validated_data['document_number'],
            'gender': serializer.validated_data['gender'],
            'email': serializer.validated_data['email']
        }

        # Check the loan data, and set is_approved according to the response
        response = requests.get(self.BASE_URL_API, params)
        loan_is_approved = response.json().get('approved', False)

        serializer.save(is_approved=loan_is_approved)


class LoanFormView(FormView):
    """
    Renders the loan application form.
    """
    form_class = LoanModelForm
    template_name = 'loans/apply.html'


class LoanListView(LoanAdminLoginMixin, ListView):
    template_name = 'loans/list.html'
    queryset = Loan.objects.filter(is_active=True)


class LoanDetailView(LoanAdminLoginMixin, DetailView):
    model = Loan
    template_name = 'loans/detail.html'


class LoanUpdateView(LoanAdminLoginMixin, UpdateView):
    model = Loan
    form_class = LoanModelForm
    template_name = 'loans/edit.html'


class LoanDeleteView(LoanAdminLoginMixin, DeleteView):
    model = Loan

    def get_success_url(self):
        return reverse('list_loans')
